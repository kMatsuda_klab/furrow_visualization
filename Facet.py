#coding:UTF-8

import numpy as np
import Vertex

class Facet:
	# essential fields
	def __init__(self, id_1, id_2, id_3):
		self.id = [id_1, id_2, id_3]

	def setVertex(self, v1, v2, v3):
		self.v = [v1, v2, v3]

	# methods
	def get_v_id(self):
		return self.id[0], self.id[1], self.id[2]

	def calNormal(self):
		loc_v1 = self.v[0].location()
		loc_v2 = self.v[1].location()
		loc_v3 = self.v[2].location()
		tmp1 = loc_v3 - loc_v2
		tmp2 = loc_v1 - loc_v3
		return np.cross(tmp1, tmp2)
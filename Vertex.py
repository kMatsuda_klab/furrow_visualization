#coding:UTF-8

import numpy as np

class Vertex:
	def __init__(self, _loc):
		self.loc = _loc
		self.vertexNormal = np.array([0.0, 0.0, 0.0])

	def addVertexNormal(self, _normal):
		self.vertexNormal += _normal

	def setVertexNormal(self):
		vN = self.vertexNormal
		length = np.linalg.norm(vN)
		if length < 1e-8:
			length += 1e-8
		self.vertexNormal = vN / length

	def location(self):
		return self.loc

	def get_vertexNormal(self):
		return self.vertexNormal
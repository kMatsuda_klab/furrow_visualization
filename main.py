#coding:UTF-8

import sys
import os
from math import *
import numpy as np
import copy

import Vertex
import Facet
import Mesh

o_vertex_locations = []
vertexNormals = []
delta_v = []
depth = []
filename_o = ""
filename_r = ""

def outputVTK(_mesh):
	global filename_o, filename_r
	global depth

	output_name = ".\\output_" + filename_o + "_" + filename_r + "\\" + _mesh.filename + "_depth_colored.vtk"
	output = open(output_name, mode="w", encoding="ascii")

	output.write("# vtk DataFile Version 2.0\n")
	output.write("depth data\n")
	output.write("ASCII\n")
	output.write("DATASET UNSTRUCTURED_GRID\n")
	output.write("POINTS {0} float\n".format(len(_mesh.vertices)))

	for i in range(len(_mesh.vertices)):
		tmp_v = _mesh.vertices[i].location()
		output.write("{0} {1} {2}\n".format(tmp_v[0], tmp_v[1], tmp_v[2]))

	num_facets = len(_mesh.facets)
	output.write("CELLS {0} {1}\n".format(num_facets, 4*num_facets))

	for i in range(num_facets):
		tmp_f = _mesh.facets[i].get_v_id()
		output.write("3 {0} {1} {2}\n".format(tmp_f[0], tmp_f[1], tmp_f[2]))

	output.write("CELL_TYPES {0}\n".format(num_facets))

	for i in range(num_facets):
		output.write("5\n")

	output.write("POINT_DATA {0}\n".format(len(_mesh.vertices)))
	output.write("SCALARS depth_color float\n")
	output.write("LOOKUP_TABLE default\n")

	for i in range(len(depth)):
		tmp_depth = depth[i]
		output.write("{0}\n".format(tmp_depth))


#main
if __name__ == '__main__':
	# set the meshes
	filename_o = input("what is the name of the original mesh? (without .obj)：　")
	filename_r = input("what is the name of the smoothed mesh? (without .obj)：　")

	original_mesh = Mesh.Mesh(filename_o)
	removed_mesh = Mesh.Mesh(filename_r)

	original_mesh_size = original_mesh.getProfiles()
	removed_mesh_size = removed_mesh.getProfiles()

	if original_mesh_size == removed_mesh_size:
		pass
	else:
		sys.exit()

	if os.path.isdir(".\\output_" + filename_o + "_" + filename_r):
		pass
	else:
		os.mkdir("output_" + filename_o + "_" + filename_r)

	# get the vertexNormals of removed_mesh and displacements of vertex locations
	vertices_size = removed_mesh_size[0]

	tmp_vertexNormals = []
	for i in range(vertices_size):
		tmp = removed_mesh.getVertexNormal(i)
		tmp_vertexNormals.append(tmp)
	vertexNormals = copy.deepcopy(tmp_vertexNormals)

	for i in range(vertices_size):
		tmp_o = original_mesh.getVertexLocation(i)
		o_vertex_locations.append(tmp_o)
		tmp_r = removed_mesh.getVertexLocation(i)
		tmp_delta = tmp_o - tmp_r
		delta_v.append(tmp_delta)

	for i in range(vertices_size):
		tmp_normal = vertexNormals[i]
		tmp_delta = delta_v[i]
		dot_product = np.dot(tmp_normal, tmp_delta)
		vertexNormals[i] *= dot_product
		depth.append(dot_product)

	outputVTK(original_mesh)
	outputVTK(removed_mesh)
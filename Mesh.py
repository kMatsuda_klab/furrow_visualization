import numpy as np

import Vertex
import Facet

class Mesh:
	def __init__(self, _filename):
		self.filename = _filename
		self.vertices = []
		self.facets = []
		self.load()
		self.setVertex()
		self.setVertexNormal()

	def load(self):
		try:
			f = open(self.filename + ".obj", "r")
		except IOError:
			print("cannot open file")
			return False

		while True:
			line = f.readline()
			if not line:
				break
			if self.processLine(line):
				pass
			else:
				pass
		f.close()
		return True

	def processLine(self, line):
		parts = line.split(" ")
		if len(parts) == 0:
			return False

		if parts[0] == "v":
			tmp = np.array([float(parts[1]), float(parts[2]), float(parts[3])])
			self.vertices.append(Vertex.Vertex(tmp))
			return True

		if parts[0] == "f":
			self.facets.append(Facet.Facet(int(parts[1])-1, int(parts[2])-1, int(parts[3])-1))
			return True

	def setVertex(self):
		for i in range(len(self.facets)):
			v_id = self.facets[i].get_v_id()
			self.facets[i].setVertex(self.vertices[v_id[0]], self.vertices[v_id[1]], self.vertices[v_id[2]])

	def setVertexNormal(self):
		for i in range(len(self.facets)):
			normal = self.facets[i].calNormal()
			v_id = self.facets[i].get_v_id()
			for j in range(3):
				self.vertices[v_id[j]].addVertexNormal(normal)
		for i in range(len(self.vertices)):
			self.vertices[i].setVertexNormal()

	def getProfiles(self):
		return len(self.vertices), len(self.facets)

	def getVertexNormal(self, _id):
		return self.vertices[_id].get_vertexNormal()

	def getVertexLocation(self, _id):
		return self.vertices[_id].location()